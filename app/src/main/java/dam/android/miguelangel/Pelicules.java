package dam.android.miguelangel;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Pelicules extends AppCompatActivity {

    private Button btDescargar;
    private Button btComentar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pelicules);

        setUI();
    }

    private void setUI(){

        btDescargar = (Button) findViewById(R.id.btDescargar);
        btComentar = (Button) findViewById(R.id.btComentar);

    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btComentar:
                Toast toast =
                        Toast.makeText(getApplicationContext(),
                                "Botón para comentar", Toast.LENGTH_SHORT);

                toast.show();
                break;
            case R.id.btDescargar:
                Toast toast1 =
                        Toast.makeText(getApplicationContext(),
                                "Botón para descargar", Toast.LENGTH_SHORT);

                toast1.show();
                break;
        }

    }
}