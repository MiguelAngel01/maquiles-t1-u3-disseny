package dam.android.miguelangel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etUser;
    private EditText etPassword;
    private Button btPelis;
    private Button btImages;
    private Button btCalc;
    private Button btForm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI(){

        etUser = (EditText) findViewById(R.id.etUser);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btPelis = (Button) findViewById(R.id.btPelis);
        btImages = (Button) findViewById(R.id.btImages);
        btCalc = (Button) findViewById(R.id.btCalc);
        btForm = (Button) findViewById(R.id.btForm);

    }

    public void changeActivity(View v) {

        if (etUser.getText().toString().equals("miguelangel") && etPassword.getText().toString().equals("1234")) {
            switch (v.getId()) {
                case R.id.btPelis:
                    Intent intent = new Intent(this, Pelicules.class);
                    startActivity(intent);
                    break;
                case R.id.btImages:
                    Intent intent1 = new Intent(this, Images.class);
                    startActivity(intent1);
                    break;
                case R.id.btCalc:
                    Intent intent2 = new Intent(this, Calculadora.class);
                    startActivity(intent2);
                    break;
                case R.id.btForm:
                    Intent intent3 = new Intent(this, Formulario.class);
                    startActivity(intent3);
                    break;
            }
        } else {
            Toast toast =
                    Toast.makeText(getApplicationContext(),
                            "User or password wrong", Toast.LENGTH_SHORT);

            toast.show();
        }

    }
}