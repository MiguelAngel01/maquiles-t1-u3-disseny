package dam.android.miguelangel;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Images extends AppCompatActivity {

    private ImageView ivPhantomThieves;
    private Button btCenter;
    private Button btCenterCrop;
    private Button btCenterIside;
    private Button btFitCenter;
    private Button btFitEnd;
    private Button btFitStart;
    private Button btFitXY;
    private Button btMatrix;
    private Button btHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);
    }

    private void setUIÇ(){

        ivPhantomThieves = (ImageView) findViewById(R.id.ivPhantomThieves);
        btCenter = (Button) findViewById(R.id.btCenter);
        btCenterCrop = (Button) findViewById(R.id.btCenterCrop);
        btCenterIside = (Button) findViewById(R.id.btCenterIside);
        btFitCenter = (Button) findViewById(R.id.btFitCenter);
        btFitEnd = (Button) findViewById(R.id.btFitEnd);
        btFitStart = (Button) findViewById(R.id.btFitStart);
        btFitXY = (Button) findViewById(R.id.btFitXY);
        btHome = (Button) findViewById(R.id.btHome);

    }

    public void onClick(View view) {

        switch (view.getId()){

            case R.id.btCenter:
                ivPhantomThieves.setScaleType(ImageView.ScaleType.CENTER);
                break;
            case R.id.btCenterCrop:
                ivPhantomThieves.setScaleType(ImageView.ScaleType.CENTER_CROP);
                break;
            case R.id.btCenterIside:
                ivPhantomThieves.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                break;
            case R.id.btFitCenter:
                ivPhantomThieves.setScaleType(ImageView.ScaleType.FIT_CENTER);
                break;
            case R.id.btFitEnd:
                ivPhantomThieves.setScaleType(ImageView.ScaleType.FIT_END);
                break;
            case R.id.btFitStart:
                ivPhantomThieves.setScaleType(ImageView.ScaleType.FIT_START);
                break;
            case R.id.btFitXY:
                ivPhantomThieves.setScaleType(ImageView.ScaleType.FIT_XY);
                break;
            case R.id.btMatrix:
                ivPhantomThieves.setScaleType(ImageView.ScaleType.MATRIX);
                break;
            case R.id.btHome:
                finish();
                break;
        }

    }
}