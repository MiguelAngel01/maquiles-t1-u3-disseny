package dam.android.miguelangel;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class Calculadora extends AppCompatActivity {

    private TextView tvResult;
    private Button bt0;
    private Button bt1;
    private Button bt2;
    private Button bt3;
    private Button bt4;
    private Button bt5;
    private Button bt6;
    private Button bt7;
    private Button bt8;
    private Button bt9;
    private Button btPlus;
    private Button btMinus;
    private Button btMult;
    private Button btDiv;
    private Button btEquals;
    private Button btBorrar;
    private Button btVolver;

    private int number1 = 0;
    private int number2 = 0;
    private char operation = '+';
    private int result = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
    }

    private void setUI() {

        tvResult = (TextView) findViewById(R.id.tvResult);
        tvResult.setText("0");
        bt0 = (Button) findViewById(R.id.bt0);
        bt1 = (Button) findViewById(R.id.bt1);
        bt2 = (Button) findViewById(R.id.bt2);
        bt3 = (Button) findViewById(R.id.bt3);
        bt4 = (Button) findViewById(R.id.bt4);
        bt5 = (Button) findViewById(R.id.bt5);
        bt6 = (Button) findViewById(R.id.bt6);
        bt7 = (Button) findViewById(R.id.bt7);
        bt8 = (Button) findViewById(R.id.bt8);
        bt9 = (Button) findViewById(R.id.bt9);
        btPlus = (Button) findViewById(R.id.btPlus);
        btMinus = (Button) findViewById(R.id.btMinus);
        btMult = (Button) findViewById(R.id.btMult);
        btDiv = (Button) findViewById(R.id.btDiv);
        btEquals = (Button) findViewById(R.id.btEquals);
        btBorrar = (Button) findViewById(R.id.btBorrar);
        btVolver = (Button) findViewById(R.id.btVolver);

    }

    public void onClickCalculadora(View view) {

        /*switch (view.getId()) {
            case R.id.bt0:
                tvResult.setText(tvResult.getText().toString() + "0");
                break;
            case R.id.bt1:
                tvResult.setText(tvResult.getText().toString() + "1");
                break;
            case R.id.bt2:
                tvResult.setText(tvResult.getText().toString() + "2");
                break;
            case R.id.bt3:
                tvResult.setText(tvResult.getText().toString() + "3");
                break;
            case R.id.bt4:
                tvResult.setText(tvResult.getText().toString() + "4");
                break;
            case R.id.bt5:
                tvResult.setText(tvResult.getText().toString() + "5");
                break;
            case R.id.bt6:
                tvResult.setText(tvResult.getText().toString() + "6");
                break;
            case R.id.bt7:
                tvResult.setText(tvResult.getText().toString() + "7");
                break;
            case R.id.bt8:
                tvResult.setText(tvResult.getText().toString() + "8");
                break;
            case R.id.bt9:
                tvResult.setText(tvResult.getText().toString() + "9");
                break;
            case R.id.btPlus:
                number1 = Integer.parseInt(tvResult.getText().toString());
                operation = '+';
                tvResult.setText("0");
                break;
            case R.id.btMinus:
                number1 = Integer.parseInt(tvResult.getText().toString());
                operation = '-';
                tvResult.setText("0");
                break;
            case R.id.btMult:
                number1 = Integer.parseInt(tvResult.getText().toString());
                operation = '*';
                tvResult.setText("0");
                break;
            case R.id.btDiv:
                number1 = Integer.parseInt(tvResult.getText().toString());
                operation = '/';
                tvResult.setText("0");
                break;
            case R.id.btEquals:
                switch (operation) {
                    case '+':
                        result = number1 + number2;
                        tvResult.setText(result);
                        break;
                    case '-':
                        result = number1 - number2;
                        tvResult.setText(result);
                        break;
                    case '*':
                        result = number1 * number2;
                        tvResult.setText(result);
                        break;
                    case '/':
                        result = number1 / number2;
                        tvResult.setText(result);
                        break;
                    default:
                        Toast toast =
                                Toast.makeText(getApplicationContext(),
                                        "Operacion no valida", Toast.LENGTH_SHORT);

                        toast.show();
                        break;
                }
                break;
            case R.id.btBorrar:
                number1 = 0;
                number2 = 0;
                result = 0;
                operation = '+';
                tvResult.setText("0");
                break;
            case R.id.btVolver:
                finish();
                break;
            default:
                Toast toast =
                        Toast.makeText(getApplicationContext(),
                                "No hay accion para ese boton", Toast.LENGTH_SHORT);

                toast.show();
                break;
        }*/

    }
}