package dam.android.miguelangel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

public class Formulario extends AppCompatActivity {

    private EditText etName;
    private EditText etPhone;
    private Spinner spArea;
    private EditText etAddress;
    private EditText etCity;
    private Spinner spState;
    private EditText etZip;
    private EditText etMail;
    private EditText etBirthday;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        setUI();
    }

    private void setUI() {

        etName = (EditText) findViewById(R.id.etName);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etAddress = (EditText) findViewById(R.id.etAddress);
        etCity = (EditText) findViewById(R.id.etCity);
        etZip = (EditText) findViewById(R.id.etZip);
        etMail = (EditText) findViewById(R.id.etMail);
        etBirthday = (EditText) findViewById(R.id.etBirthday);
        spArea = (Spinner) findViewById(R.id.spArea);
        spState = (Spinner) findViewById(R.id.spState);



    }

    public void submitData(View view) {

        String data = "Name:" + etName.getText().toString() + "\n"
                    + "Phone Number" + etPhone.getText().toString() + "\n"
                    + "Area: " + spArea.getSelectedItem().toString() + "\n"
                    + "Address: " + etAddress.getText().toString() + "\n"
                    + "City: " + etCity.getText().toString() + "\n"
                    + "State: " + spState.getSelectedItem().toString() + "\n"
                    + "Zip: " + etZip.getText().toString() + "\n"
                    + "Mail: " + etMail.getText().toString() + "\n"
                    + "Birthday: " + etBirthday.getText().toString();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(data)
                .setTitle("Data Entered");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }
}